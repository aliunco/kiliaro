//
//  Service.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation

protocol Service {
    associatedtype ResultType

    @discardableResult
    func run(callback: @escaping (_ result: Result<ResultType, Error>, _ isComplete: Bool) -> Void) -> Task?
}


/// Type-Erased Service
struct AnyService<RT>: Service {
    let _run: (@escaping (Result<RT, Error>, Bool) -> Void) -> Task?
    
    
    init <S: Service>(_ _selfie: S) where S.ResultType == RT {
        let selfie = _selfie
        
        _run = { selfie.run(callback: $0) }
    }
    
    @discardableResult
    func run(callback: @escaping (Result<RT, Error>, Bool) -> Void) -> Task? {
        return _run(callback)
    }
}

extension Service {
    
    @discardableResult
    func run(queue: DispatchQueue, _ callback: @escaping (Result<ResultType, Error>, Bool) -> Void) -> Task? {
        return run(callback: { result, isComplete in
            queue.async { callback(result, isComplete) }
        })
    }
    
    func eraseToAnyService<T>() -> AnyService<T> where T == ResultType {
        return AnyService(self)
    }
}
