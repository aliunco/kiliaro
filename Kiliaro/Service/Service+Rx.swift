//
//  Service+Rx.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import RxSwift
import Alamofire

extension Reactive where Base: Service {
    
    /// Will call base.run(queue:callback) internally
    ///
    /// - Parameter queue: Queue to call callback on from service
    func run(queue: DispatchQueue) -> Observable<Result<Base.ResultType, Error>> {
        return Observable<Result<Base.ResultType, Error>>.create { observer in
            let task = self.base.run(queue: queue) { result, isComplete in
                observer.on(.next(result))
                if isComplete {
                    observer.on(.completed)
                }
            }
            
            task?.start()
            return Disposables.create { task?.stop() }
        }
    }
}

extension Service {
    var rx: Reactive<Self> {
        return Reactive(self)
    }
}
