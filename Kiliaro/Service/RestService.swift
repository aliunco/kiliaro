//
//  RestService.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import Alamofire

extension DispatchQueue {
    static let parser = DispatchQueue(label: "com.kiliaro.test.Kiliaro.parse", qos: .default, attributes: [.concurrent])
}

struct RestService<R>: Service where R :Request {
    typealias ResultType = R.Content
    
    let session: Session?
    let request: R
    
    init (request: R, customSession: Session? = nil) {
        self.request = request
        self.session = customSession
    }
    
    private func contentParser(data: Data) throws -> R.Content {
        try JSONDecoder().decode(ServerResopnse<R.Content>.self, from: data).value
    }
    
    @discardableResult
    func run(callback: @escaping (Result<ResultType, Error>, Bool) -> Void) -> Task? {
        let callback: (Result<ResultType, Error>) -> Void = { callback($0, true) }
        let urlRequest = Result { try request.asURLRequest() }
        
        if case .failure(let error) = urlRequest {
            callback(.failure(error))
            return nil
        }
        
        return request
            .adapt(dataRequest: (self.session ?? Alamofire.Session.default).request(urlRequest.value!))
            .responseData(queue: DispatchQueue.parser) { response in
                guard response.error == nil else {
                    callback(.failure(response.error!))
                    return
                }
                
                callback(Result { try self.contentParser(data: response.data!) })
            }
    }
}
