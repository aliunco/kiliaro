//
//  GalleryItemViewModel.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation

class GalleryItemViewModel {
    
    let media: SharedMedia
    private(set) var presentingCreatedDate: String
    
    private static let presentationalFormmater: DateFormatter = {
        $0.dateFormat = "yyyy-MM-dd HH:mm:ss"
        $0.locale = .init(identifier: "en-US")
        return $0
    }(DateFormatter())
    
    init(media: SharedMedia) {
        self.media = media
        self.presentingCreatedDate = GalleryItemViewModel.presentationalFormmater.string(from: media.createdAt.date)
    }
    
    
}
