//
//  GalleryViewModel.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation
import RxSwift

class GalleryViewModel {

    // inputs
    @Observed var refresh: Bool = false
    
    // outputs
    @Observed private(set) var medias: [SharedMedia] = []
    @Observed private(set) var error: Error? = nil
    
    let disposeBag = DisposeBag()
    
    func fetchContent(reloadByForce: Bool = false) -> Single<Result<[SharedMedia], Error>> {
        return RestService(
            request: GalleryRequest(),
            customSession: reloadByForce ? nil : CachedSession().value
        )
        .eraseToAnyService()
        .rx.run(queue: .main)
        .take(1)
        .do(onNext: { [weak self] (response) in
            if (response.isSuccess) {
                self?.medias = response.value ?? []
            } else {
                self?.error = response.error
            }
        })
        .asSingle()
    }
    
}
