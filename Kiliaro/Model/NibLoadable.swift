//
//  NibLoadable.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation
import UIKit

public protocol NibLoadable: AnyObject {
    static var nibIdentifier: String { get }
}

public extension NibLoadable {
    static var nibIdentifier: String {
        return String(describing: self).components(separatedBy: ".").first!
    }
}

extension NibLoadable {
    // nib view without any owner
    static var uiNib: UINib {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: Self.nibIdentifier, bundle: bundle)
    }
}
