//
//  DefinedError.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation


protocol DefinedError: LocalizedError {
    var errorDescription: String? { get }
}

struct InvalidDate: DefinedError {}
