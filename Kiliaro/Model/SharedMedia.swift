//
//  SharedMedia.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation


enum MediaType: String, Decodable {
    case Image = "image"
}

struct SharedMedia: Decodable {
    
    let id: String
    let userId: String
    let mediaType: MediaType
    let size: Int64
    let createdAt: Date_ISO8601
    let thumbnail: URL
    let image: URL
    
    enum CodingKeys: String, CodingKey {
        case id
        case size
        case userId = "user_id"
        case mediaType = "media_type"
        case createdAt = "created_at"
        case thumbnail = "thumbnail_url"
        case image = "download_url"
    }
}
