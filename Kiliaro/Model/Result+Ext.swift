//
//  Result+Ext.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation

extension Result {
    var isSuccess: Bool {
        guard case .success = self else { return false }
        return true
    }
    
    var error: Failure? {
        guard case let .failure(error) = self else { return nil }
        return error
    }
    
    var value: Success? {
        guard case let .success(value) = self else { return nil }
        return value
    }
}
