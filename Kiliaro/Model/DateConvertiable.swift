//
//  DateConvertiable.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation

protocol DateConvertible {
    var date: Date { get }
}

extension Date: DateConvertible {
    public var date: Date { return self }
}
