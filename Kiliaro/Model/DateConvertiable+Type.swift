//
//  DateConvertiable+Type.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation

public struct Date_ISO8601: Codable, Hashable, DateConvertible {
    private let decodeValue: Date
    public var date: Date {
        return decodeValue
    }

    private static let dateFormatter: DateFormatter = {
        $0.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return $0
    }(DateFormatter())

    public init(date: Date) {
        self.decodeValue = date
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let str = try container.decode(String.self)
        if let date = Date_ISO8601.dateFormatter.date(from: str) {
            self.decodeValue = date
        }
        else {
            throw InvalidDate()
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(Date_ISO8601.dateFormatter.string(from: decodeValue))
    }
}

public struct Date_Timestamp: Codable, Hashable, DateConvertible {
    private let decodeValue: Date
    public var date: Date {
        return decodeValue
    }

    public init(date: Date) {
        self.decodeValue = date
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let timeInterval = TimeInterval(try container.decode(UInt64.self)) / 1000.0
        decodeValue = Date(timeIntervalSince1970: timeInterval)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(UInt64(decodeValue.timeIntervalSince1970 * 1000.0))
    }
}
