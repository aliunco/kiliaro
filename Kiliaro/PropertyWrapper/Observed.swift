//
//  Observed.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation
import RxSwift
import RxRelay

@propertyWrapper public struct Observed<Value> {
    @usableFromInline let wrapper: Wrapper
    @usableFromInline let scheduler: Scheduler
    
    public enum Scheduler {
        case async, sync
    }
    
    @inlinable public var wrappedValue: Value {
        get { wrapper.relay.value }
        set {
            let wrapperRelay = wrapper.relay
            // In order to prevent recursive dead lock between reaad/subscription and write
            if scheduler == .async { DispatchQueue.main.async { wrapperRelay.accept(newValue) } }
            else { wrapperRelay.accept(newValue) }
        }
    }
    
    public struct Wrapper: ObservableType {
        public typealias Element = Value
        @usableFromInline let relay: BehaviorRelay<Value>
        
        @inlinable init(initialValue value: Value) {
            self.relay = BehaviorRelay(value: value)
        }
        
        /// Use this method to become aware of changes made upon the `Value`
        /// - Important: You should retain `Dsposable` returned in order to keep receiving changes made upon the value
        /// - Parameter observer: any `Observer` type that can accept events
        @inlinable public func subscribe<Observer: ObserverType>(_ observer: Observer) -> Disposable where Observer.Element == Element {
            return self.relay.subscribe(observer)
        }
    }
    
    /// Use `projectedValue` in order to listen for changes made upon the value with method `subscribe(_:)`
    @inlinable public var projectedValue: Wrapper { wrapper }
    
    @inlinable public init(wrappedValue value: Value) {
        self.wrapper = Wrapper(initialValue: value)
        self.scheduler = .async
    }
    
    @inlinable public init(wrappedValue value: Value, scheduler: Scheduler = .async) {
        self.wrapper = Wrapper(initialValue: value)
        self.scheduler = scheduler
    }
}
