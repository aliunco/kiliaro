//
//  ViewController+Ext.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import UIKit
import RxSwift

private var associatedDisposeBag: Int8 = 0

extension UIViewController {
    // property injection for dispose bag
    var disposeBag: DisposeBag {
        MainScheduler.ensureExecutingOnScheduler()
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        guard let bag = objc_getAssociatedObject(self, &associatedDisposeBag) as? DisposeBag else {
            let bag = DisposeBag()
            objc_setAssociatedObject(self, &associatedDisposeBag, bag, .OBJC_ASSOCIATION_RETAIN)
            return bag
        }
        return bag
    }
    
    /// Returns true if ViewController is presented for first tie
    var isBeingPresentedForFirstTime: Bool {
        return isBeingPresented || isMovingToParent
    }
}
