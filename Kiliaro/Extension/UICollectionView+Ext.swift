//
//  UICollectionView+Ext.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation
import UIKit

extension UICollectionView {
    func register<N>(_: N.Type) where N: UICollectionViewCell & NibLoadable {
        self.register(N.uiNib, forCellWithReuseIdentifier: N.nibIdentifier)
    }
}
