//
//  UIImageView+Ext.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    
    func set(with url: URL, useCache: Bool = true) {
        kf.setImage(with: url, options: [useCache ? .cacheOriginalImage : .forceRefresh])
    }
    
    // MARK: Set With Placeholder
    func set(with url: URL,
             placeholder: UIImage? = nil,
             useCache: Bool = true,
             completionHandler: ((Swift.Result<UIImage, Error>) -> Void)? = nil) {
        self.image = placeholder
        
        kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [useCache ? .cacheOriginalImage : .forceRefresh],
            progressBlock: nil
        ) { [weak self] result in
            switch result {
            case .success(let kingFisherSource):
                self?.image = kingFisherSource.image
                completionHandler?(.success(kingFisherSource.image))
            case .failure(let error):
                completionHandler?(.failure(error as Error))
            }
        }
    }
    
}
