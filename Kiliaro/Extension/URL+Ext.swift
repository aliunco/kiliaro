//
//  ServerUrlImageResizer.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation

enum URLImageResizeMode: String {
    case boundingBox = "bb"
    case cropped = "crop"
    case minimumDimension = "md"
}

extension URL {
    
    func appending(queryItem: String, value: String?) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: queryItem, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems
        return urlComponents.url!
    }
    
    func imageResize(
        width: CGFloat,
        height: CGFloat,
        resizeMdoe: URLImageResizeMode = .cropped) -> URL {
            return self.appending(queryItem: "w", value: "\(Int(width))")
                .appending(queryItem: "h", value: "\(Int(height))")
                .appending(queryItem: "m", value: resizeMdoe.rawValue)
        }
    
}
