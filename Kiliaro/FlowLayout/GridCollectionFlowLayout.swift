//
//  GridCollectionFlowLayout.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import UIKit.UICollectionViewFlowLayout

class GridCollectionFlowLayout: UICollectionViewFlowLayout {
    
    public var itemsInArow: CGFloat = 3
    
    public override func prepare() {
        guard let collectionView = collectionView else { return }
        scrollDirection = .vertical
        let cellSize: CGFloat = ceil(collectionView.frame.width / itemsInArow)
        itemSize = CGSize(width: cellSize , height: cellSize)
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
    }

}
