//
//  ShareMediaGalleryRequest.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation


struct GalleryRequest: Request {
    
    typealias Content = [SharedMedia]
    
    func asURLRequest() throws -> URLRequest {
        let req = RequestBuilder(path: "shared/djlCbGusTJamg_ca4axEVw/media")
            .set(method: .get)
        return try req.asURLRequest()
    }
}
