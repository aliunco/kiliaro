//
//  RequestBuilder.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import Alamofire

class RequestBuilder: URLRequestConvertible {
    private let url: URL
    private var timeoutInterval: TimeoutInterval = .default
    private var method: HTTPMethod = .get
    private var body: Data?
    private var headers: HTTPHeaders = [:]
    private var queryItems: [URLQueryItem]? = nil
    
    public init(url: URL) {
        self.url = url
    }
    
    open func set(method: HTTPMethod) -> RequestBuilder {
        self.method = method
        return self
    }
    
    open func set(timeoutInterval: TimeoutInterval) -> RequestBuilder {
        self.timeoutInterval = timeoutInterval
        return self
    }
    
    /// - Important: Will override `Content-Type` header with `application/json`
    ///
    /// - Parameter json: JSONValue
    /// - Returns: Self
    open func set(json: JSONValue) throws -> RequestBuilder {
        self.body = try JSONEncoder().encode(json)
        headers["Content-Type"] = "application/json"
        return self
    }
    
    /// - Important: Will override `Content-Type` header with `application/json`
    ///
    /// - Parameter encodable: Encodable object to encode using Encoder
    /// - Returns: Self
    open func set<T: Encodable>(encodable: T) throws -> RequestBuilder {
        self.body = try JSONEncoder().encode(encodable)
        headers["Content-Type"] = "application/json"
        return self
    }
    
    /// Will directly set body of request
    ///
    /// - Parameter body: Data
    /// - Returns: Self
    open func set(body: Data) -> RequestBuilder {
        self.body = body
        return self
    }
    
    /// Will set query params on URL
    ///
    /// - Parameter queryItems: Array of URLQueryItem
    /// - Returns: Self
    open func set(queryItems: [URLQueryItem]) -> RequestBuilder {
        self.queryItems = queryItems
        return self
    }
    
    /// - Important: might have a conflict with `set(json: JSONValue) -> RequestBuilder`
    ///
    /// - Parameter headers: [String: String] that will be injected inside current headers
    /// - Returns: Self
    open func set(headers: HTTPHeaders) -> RequestBuilder {
        headers.forEach { self.headers[$0.name] = $0.value }
        return self
    }
    
    open func asURLRequest() throws -> URLRequest {
        let url: URL
        if let queries = queryItems,
            var urlComp = URLComponents(url: self.url, resolvingAgainstBaseURL: false) {
            urlComp.queryItems = queries
            url = urlComp.url!
        }
        else {
            url = self.url
        }
        var request = URLRequest(url: url)
        request.timeoutInterval = timeoutInterval.rawValue
        request.httpMethod = method.rawValue
        request.httpBody = body
        headers.forEach { request.setValue($0.name, forHTTPHeaderField: $0.value) }
        return request
    }
}



extension RequestBuilder {
    
    static var apiBaseUrlString: String {
        return ServerUrl.shared.rawValue
    }
    
    convenience init(path: String) {
        let url = URL(string: "\(RequestBuilder.apiBaseUrlString)\(path)")!
        self.init(url: url)
    }
}

