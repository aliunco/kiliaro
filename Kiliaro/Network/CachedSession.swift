//
//  CachedSession.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation
import Alamofire

struct CachedSession {
    
    let value: Alamofire.Session = {
        let configuration = URLSessionConfiguration.default
        
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        return Session(configuration: configuration)
    }()
    
}
