//
//  HttpMethod.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import Alamofire

public typealias HTTPMethod = Alamofire.HTTPMethod
