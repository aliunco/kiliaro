//
//  ServerUrl.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation


enum ServerUrl: String {
    case shared = "https://api1.kiliaro.com/"
}
