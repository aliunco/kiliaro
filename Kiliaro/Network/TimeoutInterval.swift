//
//  TimeoutInterval.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation

public struct TimeoutInterval: RawRepresentable {
    public typealias RawValue = Double
    
    public var rawValue: Double
    
    public init(rawValue: Double) {
        self.rawValue = rawValue
    }
    
}

extension TimeoutInterval: ExpressibleByFloatLiteral {
    public typealias FloatLiteralType = Double
    
    public init(floatLiteral: Double) {
        self.rawValue = floatLiteral
    }
}

public extension TimeoutInterval {
    static let `default`: TimeoutInterval = 15.0
}
