//
//  Request.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import Alamofire


protocol Request: URLRequestConvertible {
    associatedtype Content: Decodable
    
    func adapt(dataRequest: DataRequest) -> DataRequest
}


extension Request {
    func adapt(dataRequest: DataRequest) -> DataRequest {
        return dataRequest.validate()
    }
}
