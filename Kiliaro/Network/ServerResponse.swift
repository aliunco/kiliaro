//
//  ServerResponse.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/15/22.
//

import Foundation


struct ServerResopnse<Value: Decodable>: Decodable {
    var value: Value

    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.singleValueContainer()
        value = try valueContainer.decode(Value.self)
    }
}
