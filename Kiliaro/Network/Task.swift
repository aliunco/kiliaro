//
//  Task.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import Foundation
import Alamofire

protocol Task {
    func start()
    func stop()
}

protocol ResumableTask: Task {
    func resume()
    func pause()
}


extension Alamofire.DataRequest: Task {
    func start() {
        resume()
    }
    func stop() {
        cancel()
    }
}
