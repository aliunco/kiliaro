//
//  GalleryItemViewController.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import Foundation
import UIKit

class GalleryItemViewController: UIViewController {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var createdDateLabel: UILabel!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    //input
    var media: SharedMedia?
    
    private func presentError(error: Error) {
        let alert = UIAlertController(title: "Error!", message: "Something Bad Happend", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in alert.dismiss(animated: true) } ))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.backgroundColor = .gray
        
        guard let media = self.media else { return }
        
        activityIndicatorView.startAnimating()
        let vm = GalleryItemViewModel(media: media)
        self.imageView.set(with: media.image) {[weak self] downloadResult in
            self?.activityIndicatorView.stopAnimating()
            if let error = downloadResult.error {
                self?.presentError(error: error)
            }
        }
        self.createdDateLabel.text = vm.presentingCreatedDate
    }
    
    @IBAction func onCloseButtonTapped() {
        self.dismiss(animated: true)
    }
}
