//
//  GalleryViewController.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/14/22.
//

import UIKit
import RxCocoa
import class RxSwift.MainScheduler

class GalleryViewController: UIViewController {
    
    @IBOutlet private var collectionView: UICollectionView!
    private let viewModel = GalleryViewModel()
    private var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindViewModel()
    }
    
    private func openSharedMediaItem(media: SharedMedia) {
        
    }
    
    private func presentError(error: Error) {
        let alert = UIAlertController(title: "Error!", message: "Something Bad Happend", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in alert.dismiss(animated: true) } ))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func bindViewModel() {
        viewModel.$error
            .subscribe(onNext: { [weak self] error in
                guard let error = error else { return }
                self?.presentError(error: error)
            })
            .disposed(by: disposeBag)
        
        viewModel.fetchContent()
            .subscribe()
            .disposed(by: disposeBag)
        
        
        viewModel.$medias
            .bind(to: self.collectionView.rx.items(
                cellIdentifier: GalleryCollectionViewCell.nibIdentifier,
                cellType: GalleryCollectionViewCell.self)
            ) { index, model, cell in cell.fill(media: model) }
            .disposed(by: disposeBag)
        
        collectionView.rx
            .modelSelected(SharedMedia.self)
            .throttle(.milliseconds(500), latest: false, scheduler: MainScheduler.instance)
            .subscribe { event in
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "GalleryItemViewController") as? GalleryItemViewController else { return }
                vc.media = event.element
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }.disposed(by: disposeBag)
    }
    
    @objc func reloadDate() {
        viewModel.fetchContent(reloadByForce: true)
            .observe(on: MainScheduler.instance)
            .subscribe {[weak self] isSuccess in
                self?.refresher.endRefreshing()
            }
            .disposed(by: disposeBag)
    }
    
    private func setupView() {
        
        //making grid
        let flowLayout = GridCollectionFlowLayout()
        flowLayout.itemsInArow = 3
        collectionView.register(GalleryCollectionViewCell.self)
        collectionView.collectionViewLayout = flowLayout
        
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = .red
        self.refresher.addTarget(self, action: #selector(reloadDate), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    
    
}

