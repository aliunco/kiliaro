//
//  GalleryCollectionViewCell.swift
//  Kiliaro
//
//  Created by Ali Saeedifar on 12/16/22.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell, NibLoadable {

    @IBOutlet private weak var galleryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // round the cornders of image view
        self.galleryImage.layer.cornerRadius = 5
        self.galleryImage.clipsToBounds = true
    }
    
    func fill(media: SharedMedia) {
        let url = media.thumbnail.imageResize(
            width: ceil(self.galleryImage.frame.width),
            height: ceil(self.galleryImage.frame.width),
            resizeMdoe: .cropped
        )
        self.galleryImage.set(with: url, placeholder: UIImage(named: "placeholder"))
    }
    
    override func prepareForReuse() {
        self.galleryImage.image = nil
    }
}
